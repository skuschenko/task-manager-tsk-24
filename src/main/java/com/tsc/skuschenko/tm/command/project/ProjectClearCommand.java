package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "clear all projects";

    @NotNull
    private final String NAME = "project-clear";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        @NotNull final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        projectTaskService.clearProjects(userId);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
