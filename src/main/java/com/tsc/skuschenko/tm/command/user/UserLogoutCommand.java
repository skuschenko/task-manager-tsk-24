package com.tsc.skuschenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "logout current user";

    @NotNull
    private final String NAME = "logout";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
