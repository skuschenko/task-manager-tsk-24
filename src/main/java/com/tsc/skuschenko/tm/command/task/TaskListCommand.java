package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "show all tasks";

    @NotNull
    private final String NAME = "task-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("sort");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks = new ArrayList<>();
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        if (sort.isEmpty()) {
            tasks = taskService.findAll(userId);
        } else {
            @NotNull final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(userId, sortType.getComparator());
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
