package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "change project by index";

    @NotNull
    private final String NAME = "project-change-status-by-index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        @NotNull final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        @NotNull final IProjectService projectService
                = serviceLocator.getProjectService();
        @Nullable Project project =
                projectService.findOneByIndex(userId, valueIndex);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project = projectService.changeStatusByIndex(
                userId, valueIndex, readProjectStatus()
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
