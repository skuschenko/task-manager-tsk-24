package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyLoginException;
import com.tsc.skuschenko.tm.exception.empty.EmptyPasswordException;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (!Optional.ofNullable(roles)
                .filter(item -> item.length != 0)
                .isPresent()) return;
        @NotNull final User user =
                Optional.ofNullable(getUser()).orElseThrow(AccessDeniedException::new);
        @NotNull final Role userRole =
                Optional.ofNullable(user.getRole())
                        .orElseThrow(AccessDeniedException::new);
        for (final Role role : roles) {
            if (role.getDisplayName().equals(userRole.getDisplayName())) return;
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public User getUser() {
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(
            @Nullable final String login, @Nullable final String password
    ) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        @Nullable final Optional<User> userFind =
                Optional.ofNullable(userService.findByLogin(login));
        if (userFind.isPresent()) {
            @NotNull final User user = userFind.
                    filter(item -> item.getPasswordHash() != null &&
                            item.getPasswordHash()
                                    .equals(HashUtil.salt(password)) && !item.isLocked())
                    .orElseThrow(AccessDeniedException::new);
            userId = user.getId();
        }
    }

    @Override
    public void logout() {
        userId = null;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login, @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

}
