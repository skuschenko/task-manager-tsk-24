package com.tsc.skuschenko.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;

public interface HashUtil {

    @NotNull
    Integer ITERATION = 32154;

    @NotNull
    String SECRET = "1245654849";

    @SneakyThrows
    @Nullable
    static String md5(@Nullable final String value) {
        @Nullable String result = null;
        if (value != null) {
            @NotNull final MessageDigest md =
                    MessageDigest.getInstance("MD5");
            @Nullable final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            result = sb.toString();
        }
        return result;
    }

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

}
