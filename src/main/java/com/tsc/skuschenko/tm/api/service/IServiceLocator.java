package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    void exit();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

}
